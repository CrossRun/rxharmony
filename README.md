# RxHarmony

#### 介绍
rxjava在鸿蒙系统上的scheduler实现

#### 使用说明
```java
  Observable
                .create(new ObservableOnSubscribe<String>() {
                    @Override
                    public void subscribe(@NonNull ObservableEmitter<String> emitter) throws Throwable {
                        emitter.onNext("");
                        emitter.onComplete();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(HarmonySchedulers.mainThread())
                .subscribe(observer);
```
