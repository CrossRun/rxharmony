package top.crossrun.kdgc.oa;

import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    private static MyApplication instance;
    @Override
    public void onInitialize() {
        super.onInitialize();
        instance = this;
    }

    public static MyApplication getInstance() {
        return instance;
    }
}
